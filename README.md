# desafio full-stack

<br>

* <h3>Instalação:</h3>
Para instalar a aplicação, dentro do seu terminal, digite:

```` 
$ docker pull fabiosimini/desafio-fullstack:client

$ docker pull fabiosimini/desafio-fullstack:server

$ docker pull fabiosimini/desafio-fullstack:mysql2
```` 

Depois, dentro da pasta contendo o arquivo `docker-compose.yml`, digite:

```` 
$ docker-compose up
```` 
<br>

* <h3>Acesso:</h3>
Para acessar a aplicação, digite em seu navegador:

```` 
localhost:3000

usuário: rui.barbosa
senha: haia
```` 


<br>
Para acessar o Swagger da aplicação (contendo sua documentação), digite em seu navegador:

```` 
localhost:3001/swagger
```` 

